# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START app]
import logging
import urllib2
import json
from flask import Flask
from google.cloud import bigquery


app = Flask(__name__)


@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return 'Hello Chicago Flex!'

@app.route('/poll/segment')
def pollSegment():
    return poll('https://data.cityofchicago.org/resource/8v9j-bter.json?$limit=2000')

@app.route('/poll/region')
def pollRegion():
    return poll('https://data.cityofchicago.org/resource/t2qc-9pjd.json?$limit=2000')

def poll(url):
    try:
        result = urllib2.urlopen(url)
        trafficData = json.loads(result.read())
        return str(trafficData)
    except urllib2.URLError:
        # logging.exception('Caught exception fetching url')
        # self.response.set_status(500)
        pass

@app.route('/bigquery')
def bigQueryTest():
    bigquery_client = bigquery.Client.from_service_account_json('credentials.json')

    # The name for the new dataset
    dataset_id = 'my_new_dataset'

    # Prepares a reference to the new dataset
    dataset_ref = bigquery_client.dataset(dataset_id)
    dataset = bigquery.Dataset(dataset_ref)

    # Creates the new dataset
    dataset = bigquery_client.create_dataset(dataset)

    print('Dataset {} created.'.format(dataset.dataset_id))

    listOfTables = list(bigquery_client.list_datasets())
    return str(listOfTables)

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [END app]
